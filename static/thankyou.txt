Thank you for the music, thank you for my life
Thank you for the good, bad, wrong and the right
Thank you for my laughter, thank you for my name
Thank you for the love that grows in my days
Thank you for my sunshine, thank you for my clouds
Thank you for my yesterdays, thank you for right now
