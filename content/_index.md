![bar](/bar.gif)

hi, i'm worf. welcome to my site

i'm picking up photography. i live in melbourne, australia (yep there are giant poisonous spiders crawling up my arm right now as i type this)

![bar](/bar.gif)

### things i like

- my cat and dog
- listening to music ([my last.fm](https://last.fm/user/worf1337))
- occasionally playing games
- wasting money on useless stuff
- wondering why i have no money
- taking photos ([pixelfed](https://justsome.photos/worf))

![bar](/bar.gif)

### contacts

- email is <a href="mailto:worf@cocaine.ninja">worf@cocaine.ninja</a>. my old email, me@worf.win, is no longer active due to network changes. emails sent there will be redirected to my new one.
    - if you want encryption my PGP key is available [here](/WorfPublic.asc)
        - fingerprint is `048B4C8E65F788F0CF17C6B4FD1F174F98394001`

- my fedi handle is available on request via email (stops weirdos)

- i also have matrix, my handle is available on request through any of the other above contact methods

- i currently don't accept requests on any other platforms, sorry

- when interacting with me please be mindful that i am a minor. that obviously means no flirting or anything that would be considered inappropriate for someone under 18.

![bar](/bar.gif)
