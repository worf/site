---
title: "Restoring an M1 Mac over DFU mode on Linux"
date: 2023-12-06T23:13:55+11:00
draft: false
---

note: This is my first ever guide. Please let me know if something needs improving on my [contact page](/contact).

When an M1 Mac's lower level firmware somehow gets corrupted (e.g by shutting the computer down while it's still installing), it goes into a really ugly recovery screen with an exclamation mark, linking to a [support page](https://support.apple.com/mac/restore). The thing is, it says you need to have another Mac to do it. Unfortunately not everyone has a bathtub of money, so not everyone has 2 Macs.

This exact scenario happened to me with my school MacBook when I was trying to downgrade to Monterey.

Luckily the amazing [idevicerestore](https://github.com/libimobiledevice/idevicerestore) project now has support for restoring M1 Macs. Now all you really need is a Linux machine, a USB-A to USB-C cable and a bit of patience.

# First Steps

First off, install the development tools of whatever distribution you have. You'll need a compiler, make, all that stuff. Oh, and you'll need git as well, because we'll be installing the git versions of various packages (stable releases are sorta rare and also the current one can't restore M1 Macs, yet).

Also, install libplist and its development headers (called libplist-dev on Debian-based distros) with the package manager of whatever distro you're using.

# Cloning the repos

Now clone the following repos:

`git clone https://github.com/libimobiledevice/idevicerestore`  
`git clone https://github.com/libimobiledevice/libimobiledevice`  
`git clone https://github.com/libimobiledevice/libimobiledevice-glue`  
`git clone https://github.com/libimobiledevice/libirecovery`  
`git clone https://github.com/libimobiledevice/libusbmuxd`  
`git clone https://github.com/libimobiledevice/usbmuxd`  

We'll be compiling these in a specific order, because some of these depend on each other.

# Compiling

We'll be doing this in the following order:

    1. libimobiledevice-glue
    2. libusbmuxd
    3. usbmuxd
    4. libimobiledevice
    5. libirecovery
    6. idevicerestore

Go into each directory in order, and run `./autogen.sh`. After that, run `make` and `sudo make install`.

For example, to compile and install libimobiledevice I would do:

```
cd libimobiledevice
./autogen.sh
make
sudo make install
```
# Doing it

```
YOU WILL LOSE ALL YOUR DATA. BACK UP ANYTHING IMPORTANT.
```

Alright, now that we've compiled and installed everything we need, it's time to actually restore the Mac. Put the Mac into DFU mode (Google search this), and plug the USB-C cable into the leftmost port.

Open a terminal, and run `sudo /usr/local/sbin/usbmuxd -f -p`. Open another terminal, and run `sudo idevicerestore -ld`. It will give you a list of versions you can go to. Pick any you want. After this, your Mac will restore and boot into the setup screen.

Hope this helps you and your wallet. Next time don't be like me and hold the power button while macOS is installing. Thanks for reading.
    
